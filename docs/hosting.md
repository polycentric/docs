---
label: Hosting A Server
icon: server
---

## Quick start: How to run a Polycentric server

Polycentric's hosting centers around docker-compose. We believe docker-compose strikes the right balance between simplicity and flexibility. In the future, it will be possible to run more complex configurations, but these will still be based on docker-compose.

Following these instructions requires `docker`, `docker-compose`, and `git`.

```bash
git clone https://gitlab.futo.org/polycentric/polycentric.git
cd polycentric
mkdir -p state/opensearch/data
sudo chown 1000:1000 -R state/opensearch/data
```

### Basic Configuration

1. Create a `.env` file in your project root to store sensitive configuration:

```bash
# Admin token for server management
ADMIN_TOKEN=your_strong_password_here
# PostgreSQL password
POSTGRES_PASSWORD=your_database_password_here
```

2. Edit `Caddyfile` replacing `srv1.polycentric.io` with your domain name. Go to your domain registrar and set an `A`, and or `AAAA` record pointing to your server. Caddy will automatically fetch a TLS certificate for your domain when the server starts.

### Start the server

```bash
docker-compose -f docker-compose.production.yml up -d
```

You are now done. Add your new server to your profile using the Polycentric client and start using it. Upgrading the server is very simple:

```bash
# stop the existing server
docker-compose -f docker-compose.production.yml down
# download updates
docker-compose -f docker-compose.production.yml pull
# start the server up again
docker-compose -f docker-compose.production.yml up -d
```

## Advanced server management

A Polycentric server depends on `PostgreSQL`, and `OpenSearch`. Configuration is controlled with environment variables. A proxy such as `NGINX`, or `Caddy` is required to provide TLS. Manual configuration is required if you wish to run a distributed server, such as storage being run on other nodes. The Polycentric server may be horizontally scaled across multiple nodes with shared external storage.

### Core Environment Variables

```bash
# An unsigned 16 bit number representing the port that should be bound.
export HTTP_PORT_API='80'
# The PostgreSQL connection string
export POSTGRES_STRING='postgres://postgres:testing@postgres'
# The OpenSearch connection string
export OPENSEARCH_STRING='http://opensearch:9200'
# The token required for administrative tasks
export ADMIN_TOKEN='something_long_and_random'
```

## Optional Components

### Machine learning moderation tagging (recommended)

Polycentric supports machine learning moderation tagging, where posts are tagged by different categories- currently typically sexual content, violence, and hate speech. There is also auto-CSAM detection and deletion. 

There are three modes for enabling machine learning moderation tagging:

1. `OFF`: This disables machine learning moderation tagging and ignores existing moderation tags.
2. `LAZY` (default): This adds moderation tags to posts, but also shows posts before they are tagged, meaning that posts may be shown that are not appropriate until they are processed off the queue. This mode is helpful for adding moderation to an existing server without making all content unavailable until it is processed.
3. `STRONG`: This adds moderation tags to posts, and does not show posts until they are tagged.

You can set the mode with the following environment variable:

```bash
MODERATION_MODE=LAZY
```

To enable machine learning moderation tagging, you must also set a tagging provider. Currently, we only support 'azure' for tagging, but it's very easy to add a new provider. If someone is interested, we think Meta's new [llama-guard](https://huggingface.co/meta-llama/Llama-Guard-3-11B-Vision) model would be a good self-hosted option with the same performance. It's also hosted on [Groq](https://groq.com/pricing/) (no vision as of writing).

For Azure, please add the following environment variables:

```bash
TAG_INTERFACE=azure
AZURE_TAGGING_ENDPOINT=your_azure_endpoint
AZURE_TAGGING_SUBSCRIPTION_KEY=your_azure_subscription_key
AZURE_TAGGING_API_VERSION=your_azure_api_version
```


For CSAM detection, currently we only support PhotoDNA. To enable PhotoDNA, you must set the following environment variables:

```bash
CSAM_INTERFACE=photodna
PHOTODNA_KEY=your_photodna_key
```

You can also adjust the rate limits for tagging and CSAM detection with the following environment variables, which have default values but can be adjusted to your needs:

```bash
TAGGING_REQUEST_RATE_LIMIT=10
CSAM_REQUEST_RATE_LIMIT=4
```

### Verifiers Service

The verifiers service allows users to verify their identities through various social platforms (Discord, Instagram, X/Twitter). This is optional and can be added to your configuration if needed:

```bash
# Add to your .env file if using verifiers:
OAUTH_CALLBACK_DOMAIN=https://your-verifier-domain.com
DISCORD_CLIENT_ID=your_discord_client_id
DISCORD_CLIENT_SECRET=your_discord_client_secret
# Additional social platform credentials as needed
```

### Manual Moderation API (different from automoderation)

Server operators may choose to censor content on their own node. There are two types of censorship available. The first `DO_NOT_RECOMMEND` means that content will not be returned in server curated data. Examples of this include the explore page, and recommended profiles. The second is `DO_NOT_STORE` where the server will outright refuse to host data.

A specific post, or entire profile may be censored. The API accepts a particular link to censor. If you want to censor a post, provide a link to a post, if you want to censor a profile, provide a link to a profile.

Example API usage with curl:

```bash
curl \
    -X POST \
    -H 'Authorization: abc123' \
    https://my-server.com/censor?censorship_type=DO_NOT_RECOMMEND \
    -d 'https://polycentric.io/feed/a/CiA_zaEPAlQ2H7hmNbT'
```
