---
label: FUTO ID
icon: rocket 
---

# FUTO ID
Formerly known as Harbor.

## Overview

FUTO ID is a powerful identity management system built on Polycentric that helps you establish and verify your online presence across platforms. With FUTO ID, you can link your various online identities, make verifiable claims, and build a web of trust through user endorsements.

Similar to the [PGP Web of Trust](https://en.wikipedia.org/wiki/Web_of_trust), FUTO ID creates a decentralized network of verified identities:

> As time goes on, you will accumulate keys from other people that you may want to designate as trusted introducers. Everyone else will each choose their own trusted introducers. And everyone will gradually accumulate and distribute with their key a collection of certifying signatures from other people, with the expectation that anyone receiving it will trust at least one or two of the signatures. This will cause the emergence of a decentralized fault-tolerant web of confidence for all public keys.

You can view FUTO ID profiles at [harbor.social](https://harbor.social) and manage your identity using the FUTO ID apps for [iOS](https://apps.apple.com/us/app/futo-id/id6456984783) and [Android](https://play.google.com/store/apps/details?id=org.futo.harbor_flutter).

## Identity Linking

FUTO ID lets you connect your identity to accounts across the internet through a claim and verification process. When you make a claim of account ownership, other users—including automated verification bots—can endorse that claim. Verification bots can automatically endorse claims when you demonstrate control of an account through one of two challenge methods.

### Challenge Methods

#### Crawler Challenge
With the crawler challenge method, you'll generate a unique token derived from your public key and place it in a location you control, like your account description. A verification bot then checks for this token using a web scraper. This method works best with platforms that don't heavily restrict web crawling.

#### OAuth Challenge
The OAuth challenge uses official platform login systems. When you choose to "Log in with X platform", you'll authenticate directly with the service, which then confirms your identity to the verification bot. Your password remains secure as the bot never receives it. This method is ideal for platforms with strong anti-crawling measures that support OAuth.

### Supported Platforms

| Platform     | Domain                                               | Challenge method |
|--------------|------------------------------------------------------|------------------|
| Discord      | [discord.com](https://discord.com)                   | OAuth            |
| Github       | [github.com](https://github.com)                     | Crawler          |
| Gitlab       | [gitlab.com](https://gitlab.com)                     | Crawler          |
| Hacker News  | [news.ycombinator.com](https://news.ycombinator.com) | Crawler          |
| Instagram    | [instagram.com](https://instagram.com)               | OAuth            |
| Kick         | [kick.com](https://kick.com)                         | Crawler          |
| Minds        | [minds.com](https://minds.com)                       | Crawler          |
| Nebula       | [nebula.tv](https://nebula.tv)                       | Crawler          |
| Odyssee      | [odysee.com](https://odysee.com)                     | Crawler          |
| Patreon      | [patreon.com](https://patreon.com)                   | Crawler          |
| Rumble       | [rumble.com](https://rumble.com)                     | Crawler          |
| Spotify      | [spotify.com](https://spotify.com)                   | OAuth            |
| Spreadshop   | [spreadshop.com](https://spreadshop.com)             | Crawler          |
| Substack     | [substack.com](https://substack.com)                 | Crawler          |
| Twitch       | [twitch.tv](https://twitch.tv)                       | Crawler          |
| Vimeo        | [vimeo.com](https://vimeo.com)                       | Crawler          |
| Website      | [example.com](https://example.com)                   | Crawler          |
| X / Twitter  | [x.com](https://x.com)                               | OAuth            |
| Youtube      | [youtube.com](https://youtube.com)                   | Crawler          |

## Running a Verifier

To run your own verification service, use our Docker image at `gitlab.futo.org:5050/videostreaming/verifiers:latest`. You'll need to:

1. Expose the configured port
2. Mount a volume to `/usr/src/app/state` for data persistence
3. Configure the following environment variables:

| Variable                | Default   |
|-------------------------|-----------|
| PORT                    | 3000      |
| OAUTH_CALLBACK_DOMAIN   | undefined |
| DISCORD_CLIENT_ID       | undefined |
| DISCORD_CLIENT_SECRET   | undefined |
| INSTAGRAM_CLIENT_SECRET | undefined |
| INSTAGRAM_CLIENT_ID     | undefined |
| X_CLIENT_ID             | undefined |
| X_CLIENT_SECRET         | undefined |
| X_API_KEY               | undefined |
| X_API_SECRET            | undefined |


## Source Code

Find our open-source code in the following repositories:

| Application      | Link                                                                                |
|------------------|-------------------------------------------------------------------------------------|
| FUTO ID Web       | <https://gitlab.futo.org/polycentric/polycentric/-/tree/master/packages/harbor-web> |
| FUTO ID App       | <https://gitlab.futo.org/polycentric/harbor>                                        |
| FUTO ID Verifiers | <https://gitlab.futo.org/videostreaming/verifiers>                                  |
| FUTO ID Gateway   | <https://gitlab.futo.org/polycentric/harbor-gateway>                                |