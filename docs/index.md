---
label: Introduction
icon: home
---

# Introduction

Polycentric is an open-source, distributed social network that lets you publish content to multiple servers. If you’re censored on one server, your content remains accessible from other servers. It features an innovative topic system where you can easily join discussions on specific keywords or URLs, and an opt-out moderation model that allows you to configure how much (or how little) moderation you want to see.

Check out our [GitLab repository](https://gitlab.futo.org/polycentric/polycentric) or jump right into the [app](https://polycentric.io).

---

## Why Polycentric?

### Distributed Architecture

Polycentric’s decentralized design ensures that no single server or organization controls the platform. Instead of relying on a central authority, data is spread across many servers. If one server attempts to censor content, Polycentric automatically checks other servers to retrieve that content, helping ensure it remains accessible.

### Opt-Out Moderation

You can choose how much moderation you want to see. Each server curates its own recommendation feeds—helpful for filtering spam or NSFW content—but users are free to pick which servers they follow and how much content gets filtered.

### Self-Sovereign Identities

Your identity belongs to you, not to a platform. On Polycentric, each user’s profile is cryptographically secured. This means posts can’t be altered by a server, and nobody can delete a user from the network as long as at least one node continues to host that user’s data.

---

## Cryptography

Each message is signed by the user’s own cryptographic keys, ensuring authenticity and integrity. No server ever has access to these keys. You can link multiple devices or move your profile around seamlessly because you remain in control of your data and digital identity.

---

## Data Storage

Polycentric uses a distributed approach to file storage. Posts or media can be fetched from any server holding a copy, keeping the network resilient—even if certain nodes go offline. This lowers the risk of downtime and helps ensure efficient file sharing at scale.

---

## Why Not Use Blockchain?

While blockchain-based solutions excel in certain financial use cases, they aren’t always optimal for low-latency, high-throughput applications. Polycentric provides similar benefits—such as distributed data synchronization and censorship detection—without the computational overhead or delays of blockchain consensus mechanisms.

---

## Open Source

All Polycentric code is open source, allowing anyone to inspect, audit, or contribute to it. You can also run your own instance or experiment in test environments without reliance on any central authority or vendor lock-in.

---

For more information, explore our [GitLab repository](https://gitlab.futo.org/polycentric/polycentric) or try out the [Polycentric app](https://polycentric.io).
