---
label: Getting Started
icon: rocket
description: Includes information on build systems, development workflow, and common issues.
---

# Polycentric Development Guide 
Includes information on build systems, development workflow, and common issues.

## Project Overview
Polycentric is a distributed social network with a monorepo structure containing several key packages:

- `polycentric-core`: Core functionality and protocol implementation
- `polycentric-react`: React components and UI library 
- `polycentric-web`: Web application
- `polycentric-capacitor`: Mobile application
- `polycentric-desktop`: Electron application

## Initial Setup

1. Install dependencies:
```
npm install
```

2. Generate development certificates (required for HTTPS):
```
make devcert
```

3. Generate protocol files:
```
make proto
```

## Development Workflow

### 1. Start Core Package
The core package needs to be running first since other packages depend on it:

```
cd packages/polycentric-core
npm run dev
```

This runs multiple concurrent processes:
- TypeScript declaration file generation (watch mode)
- ESM bundle generation (watch mode)
- CommonJS bundle generation for browser (watch mode)
- CommonJS bundle generation for Node.js (watch mode)

### 2. Start React Package
The React package contains all UI components:

```
cd packages/polycentric-react  
npm run dev
```

Key Vite configuration:
- Development mode excludes `@polycentric/polycentric-core` from dependencies
- Builds both ES and UMD formats
- Includes asset handling via `vite-plugin-lib-assets`
- Integrates Tailwind CSS processing

### 3. Start Web/Capacitor Application
Finally, start the web or mobile app:

```
cd packages/polycentric-web
npm run dev
```

The web app's Vite config is specially configured to:
- Exclude polycentric dependencies in development
- Use HTTPS with custom certificates
- Configure PWA manifest
- Set up manual chunk splitting

## Development Tips

1. Always run packages in order (core → react → web/capacitor)
2. Changes in core automatically propagate to dependent packages
3. Use HTTPS in development with generated certificates to ensure full browser API access

## Common Issues

1. Module Resolution Errors
   - Ensure all packages are running in development mode
   - Check that core package is built and running

2. HTTPS Certificate Errors  
   - Run `make devcert` to generate certificates
   - Trust the certificates in your browser

3. Changes Not Reflecting
   - Verify all watch processes are running
   - Check console for build errors

4. Dependency Issues
   - Run `npm install` from root directory
   - Clear node_modules and reinstall if needed

## Additional Resources

- Full documentation: https://docs.polycentric.io
- Live demo: https://polycentric.io
- Source code: https://gitlab.futo.org/polycentric/polycentric