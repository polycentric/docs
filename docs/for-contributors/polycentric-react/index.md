---
label: Polycentric React
icon: browser
---

# Polycentric React

Polycentric React is a full implementation of the Polycentric protocol that is ported to modern clients for social networking. Independent components can be used independently, however we don't recommend it as components are tied to the needs of the social networking experience.