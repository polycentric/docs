---
label: Polycentric Core
icon: gear
---

The `polycentric-core` package serves as the foundational library for the Polycentric distributed social network. It provides the core functionality and data models used across different Polycentric clients and implementations.

Polycentric Core is written in TypeScript and implements the core protocol, cryptographic operations, data models, and querying capabilities needed for the distributed social network.
