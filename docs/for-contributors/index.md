---
label: For Contributors
icon: code
---

Want to contribute to Polycentric? Here's how our codebase is structured.

Polycentric is a monorepo. The codebase is split into a TypeScript section managed with npm workspaces, and a Rust section managed with cargo workspaces. All client code aside from [our capacitor plugin for LevelDB](https://gitlab.futo.org/polycentric/leveldb-capacitor-plugin) is in the TypeScript section.

The TypeScript section is split into a core library, a react library, and different end client targets - namely web, capacitor, and electron. The core library handles fetching and data persistence, and is used by the react library and different end client targets. Any data structure that supports the abstract-level interface can be used by polycentric-core to persist data, whether it be in-memory or on disk. You'll notice each end client has different persistence mechanisms.