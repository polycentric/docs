---
label: Server
icon: server
---

# Polycentric Server Architecture

Polycentric uses the Tokio runtime in the Rust programming language for its server. We use postgres as our database, although any columar database should work. We use OpenSearch as our search engine, and `warp` as our web framework. The server codebase is pretty straightforward, and the [main.rs](https://gitlab.futo.org/polycentric/polycentric/-/blob/master/server/src/main.rs) file is the entry point.