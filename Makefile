.PHONY: build-docker-image push-docker-image join-sandbox

tag = gitlab.futo.org:5050/polycentric/docs

build-docker-image:
	docker build -t $(tag) - < Dockerfile

push-docker-image:
	docker push $(tag)

join-sandbox:
	docker run --rm -it \
		--user "$(shell id -g):$(shell id -u)" \
		--cap-drop='ALL' \
		--security-opt='no-new-privileges' \
		--memory='512m' \
		--cpus='0.5' \
		--workdir='/app' \
		--publish='127.0.0.1:8083:8083/tcp' \
		--mount type=bind,source="$(PWD)",target='/app' \
		$(tag) \
		/bin/bash
